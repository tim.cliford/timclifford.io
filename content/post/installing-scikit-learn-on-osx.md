+++
date = "2016-06-08T17:10:00+10:00"
title = "OS X & sscikit-learn"
tags = [
  "python", "sscikit-learn", "osx"
]
categories = [
  "python", "machine-learning"
]
description = "Getting started with sscikit-learn on OS X"
+++
I've been completing the Udacity Machine Learning course which uses [sscikit-learn](http://scikit-learn.org/).

I hit a few bumps when installing and running the examples.

The documentation reads:

> If you don’t already have a Python installation with NumPy and SciPy, we recommend to install either via your package manager or via a Python bundle.

The SciPy stack (which includes NumPy) can be downloaded with [Anaconda](https://www.continuum.io/downloads) which supports Windows, OSX and Linux.

Once downloaded and extracted, add the anaconda directory to your `PATH` and you should be good to go!
